import * as monaco from 'monaco-editor';
import { editor, languages, KeyMod, KeyCode } from 'monaco-editor';
import { fromEvent, Observable, merge, throwError, of, lastValueFrom } from 'rxjs';
import { shareReplay, debounceTime, skip, distinctUntilChanged, map, filter, share, switchMap, take, mergeMap, tap, retry } from 'rxjs/operators';

import fileDialog from 'file-dialog';
import { saveAs } from 'file-saver';
import CssDom from 'cssdom';
import * as base64 from './base64.js';

import { getXmlCompletionProvider, getXmlHoverProvider } from '../lib/editsvgcode/src/completion-provider.js';

import XmlBeautifier from 'xml-beautify';

import { gzip, ungzip } from 'pako';

languages.registerCompletionItemProvider( 'xml', getXmlCompletionProvider( monaco ) );
languages.registerHoverProvider( 'xml', getXmlHoverProvider( monaco ) );

function *traverse( node: Node ): IterableIterator<Node> {
	if( node == null ) return;
	yield node;
	yield *traverse( node.firstChild );
	yield *traverse( node.nextSibling );
}

function formatXml( xml: string, model: editor.ITextModel ) {
	const options = model.getOptions();
	const indent = options?.insertSpaces ? ' '.repeat( options?.indentSize ?? 2 ) : '\t';
	try {
		const doc = ( new DOMParser ).parseFromString( xml, 'image/svg+xml' );
		for( const node of traverse( doc.documentElement ) ) {
			if( node.nodeType === node.ELEMENT_NODE ) {
				const el = node as Element;
				if( el.localName === 'style' ) {
					try {
						const cssDom = new CssDom( el.textContent );
						while( el.firstChild ) el.removeChild( el.firstChild );
						el.appendChild( el.ownerDocument.createCDATASection( cssDom.beautify( { indent } ) ) );
					} catch {}
				}
			}
		}
		xml = ( new XMLSerializer ).serializeToString( doc );
	} catch {}
	const xmlBeautifier = new XmlBeautifier;
	return xmlBeautifier.beautify( xml, {
		indent,
		useSelfClosingElement: true
	} );
}

languages.registerDocumentFormattingEditProvider( 'xml', {
	provideDocumentFormattingEdits( model ) {
		const range = model.getFullModelRange();
		return [ {
			range,
			text: formatXml( model.getValueInRange( range ), model )
		} ];
	}
} );

const options = {
	tabSize: 2,
	indentSize: 2,
	insertSpaces: false,
	trimAutoWhitespace: true
};

self[ 'MonacoEnvironment' ] = {
	getWorkerUrl( _moduleId, label ) {
		switch( label ) {
		case 'editorWorkerService': return './editor.worker.js';
		default: throw new Error( `Unknown worker label ${label}` );
		}
	}
};

const storage = localStorage;
const sender = 'index.ts';

async function showDiv( div: HTMLElement, text: string ) {
	div.textContent = text;
	div.style.animation = '';
	setTimeout( () => {
		div.style.animation = 'fadeOut 2s ease-out';
	}, 0 );
}

interface ToolbarInfo {
	readonly editor: editor.ICodeEditor;
	readonly status: HTMLDivElement;
}
const toolbarInfoMap = new Map<Element, ToolbarInfo>();

( async () => {
	const divEditor = document.getElementById( 'editor' );
	const editorStatus = document.createElement( 'div' );
	editorStatus.className = 'status';

	let filename = storage.getItem( 'filename' ) ?? 'untitled.svg';
	const svgEditor = editor.create( divEditor, {
		autoClosingBrackets: 'never',
		autoClosingQuotes: 'never',
		value: storage.getItem( 'content' ) ?? `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
</svg>
`,
		theme: 'vs-dark',
		language: 'xml',
		automaticLayout: true,
		formatOnPaste: true,
		formatOnType: false,
		renderWhitespace: 'boundary'
	} );

	function loadHashData() {
		if( location.hash.length <= 1 ) return;
		try {
			const data = ungzip( base64.decode( location.hash.replace( /^#/, '' ) ), { to: 'string' } );
			const nulIndex = data.indexOf( '\0' );
			if( nulIndex < 0 ) return;
			filename = data.substr( 0, nulIndex );
			svgEditor.setValue( data.slice( nulIndex + 1 ) );
			location.href = '#';
			showDiv( editorStatus, `Loaded ${filename}` );
		} catch {}
	}

	loadHashData();

	fromEvent( window, 'hashchange' )
	.subscribe( () => {
		loadHashData();
	} );

	const editorToolbar = document.getElementById( 'editor-toolbar' );
	svgEditor.addOverlayWidget( {
		getDomNode: () => editorToolbar,
		getId: () => 'toolbar',
		getPosition: () => ( {
			preference: editor.OverlayWidgetPositionPreference.TOP_RIGHT_CORNER
		} )
	} );
	svgEditor.addOverlayWidget( {
		getDomNode: () => editorStatus,
		getId: () => 'status',
		getPosition: () => null
	} );
	const model = svgEditor.getModel();
	model.updateOptions( options );

	const svgSource = editor.create( document.getElementById( 'source' ), {
		theme: 'vs-dark',
		renderWhitespace: 'boundary',
		codeLens: false,
		lineDecorationsWidth: 0,
		automaticLayout: true,
		lineNumbers: 'off',
		minimap: {
			enabled: false
		},
		readOnly: true,
		wordWrap: 'on'
	} );
	const sourceToolbar = document.getElementById( 'source-toolbar' );
	const sourceStatus = document.createElement( 'div' );
	sourceStatus.className = 'status';
	svgSource.addOverlayWidget( {
		getDomNode: () => sourceStatus,
		getId: () => 'status',
		getPosition: () => ( {
			preference: editor.OverlayWidgetPositionPreference.TOP_CENTER
		} )
	} );
	svgSource.addOverlayWidget( {
		getDomNode: () => sourceToolbar,
		getId: () => 'toolbar',
		getPosition: () => null
	} );
	svgSource.getModel().updateOptions( options );

	toolbarInfoMap.set( editorToolbar, { editor: svgEditor, status: editorStatus } );
	toolbarInfoMap.set( sourceToolbar, { editor: svgSource, status: sourceStatus } );

	const command$ =
	fromEvent( document.documentElement, 'click' )
	.pipe(
		map( event => {
			const element = ( event.target as Element ).closest( '[data-command]' );
			const command = element?.getAttribute( 'data-command' );
			const toolbar = element?.closest( '.toolbar' );
			const { editor, status } = toolbarInfoMap.get( toolbar ) ?? {};
			return { event, element, command, toolbar, editor, status };
		} ),
		filter( e => e.command != null ),
		share()
	);

	command$
	.pipe( filter( ( { command } ) => command === 'copy' ) )
	.subscribe( ( { event, editor, status } ) => {
		event.preventDefault();
		const sel = editor.getSelections();
		if( !sel.some( s => !s.isEmpty() ) ) {
			editor.setSelection( editor.getModel().getFullModelRange() );
		}
		editor.trigger( sender, 'editor.action.clipboardCopyAction', {} );
		editor.setSelections( sel );
		showDiv( status, 'Copied to clipboard' );
	} );

	command$
	.pipe( filter( ( { command } ) => command === 'format' ) )
	.subscribe( ( { event, status } ) => {
		event.preventDefault();
		svgEditor.trigger( sender, 'editor.action.formatDocument', {} );
		showDiv( status, 'Formatted document' );
	} );

	command$
	.pipe( filter( ( { command } ) => command === 'share' ) )
	.subscribe( ( { event, editor, status } ) => {
		event.preventDefault();
		const value = editor.getValue();
		const data = gzip( `${filename}\0${value}`, { level: 9 } );
		const url = new URL( '#' + base64.encode( data ), location.href );
		const viewState = editor.saveViewState();
		editor.setSelection( { startColumn: 0, startLineNumber: 0, endColumn: 0, endLineNumber: 0 } );
		editor.setValue( url.href );
		editor.trigger( sender, 'editor.action.clipboardCopyAction', {} );
		editor.setValue( value );
		editor.restoreViewState( viewState );
		showDiv( status, 'Link copied to clipboard' );
	} );

	async function loadFile( editor: editor.ICodeEditor, status: HTMLDivElement, file: File ) {
		const reader = new FileReader;
		const promise = lastValueFrom( merge(
			fromEvent( reader, 'load' ),
			merge(
				fromEvent( reader, 'abort' ),
				fromEvent( reader, 'error' )
			)
			.pipe( switchMap( e => throwError( () => e ) ) )
		).pipe(
			take( 1 ),
			map( () => reader.result )
		) );
		reader.readAsText( file );
		const data = String( await promise );
		filename = file.name || 'untitled.svg';
		storage.setItem( 'filename', filename );
		showDiv( status, `Loaded ${filename}` );
		editor.setValue( data );
	}

	async function load( editor: editor.ICodeEditor, status: HTMLDivElement ) {
		const file = ( await fileDialog( {
			accept: 'image/svg+xml',
			multiple: false
		} ) )?.item( 0 );
		if( file == null ) return;
		await loadFile( editor, status, file );
	}

	async function save( editor: editor.ICodeEditor, status: HTMLDivElement ) {
		const blob = new Blob( [
			editor.getValue()
		], { type: 'image/svg+xml' } );
		saveAs( blob, filename );
		showDiv( status, `Saved ${filename}` );
	}

	svgEditor.addCommand( KeyMod.CtrlCmd | KeyCode.KeyO, () => { load( svgEditor, editorStatus ); } );
	svgEditor.addCommand( KeyMod.CtrlCmd | KeyCode.KeyS, () => { save( svgEditor, sourceStatus ); } );

	const key$ = fromEvent<KeyboardEvent>( document.documentElement, 'keypress' ).pipe( share() );

	key$
	.pipe( filter( e => e.key?.toLowerCase() === 'l' && e.ctrlKey ) )
	.subscribe( async e => {
		e.preventDefault();
		await load( svgSource, sourceStatus );
	} );

	key$
	.pipe( filter( e => e.key?.toLowerCase() === 's' && e.ctrlKey ) )
	.subscribe( async e => {
		e.preventDefault();
		await save( svgSource, sourceStatus );
	} );

	of( 'dragenter', 'dragover' )
	.pipe( mergeMap( e => fromEvent<DragEvent>( divEditor, e ) ) )
	.subscribe( e => {
		e.preventDefault();
		e.stopPropagation();
	} );

	fromEvent<DragEvent>( divEditor, 'drop' )
	.subscribe( async e => {
		e.preventDefault();
		e.stopPropagation();
		const file = e.dataTransfer?.files?.[ 0 ];
		if( file == null ) return;
		await loadFile( svgEditor, sourceStatus, file );
	} );

	command$
	.pipe( filter( ( { command } ) => command === 'load' ) )
	.subscribe( async ( { event, editor } ) => {
		event.preventDefault();
		await load( editor, editorStatus );
	} );

	command$
	.pipe( filter( ( { command } ) => command === 'save' ) )
	.subscribe( async ( { event, editor } ) => {
		event.preventDefault();
		await save( editor, editorStatus );
	} );


	const content = new Observable<string>( observer => {
		observer.next( model.getValue() );
		model.onDidChangeContent( () => {
			observer.next( model.getValue() );
		} );
		return () => {};
	} ).pipe( shareReplay( 1 ), distinctUntilChanged(), debounceTime( 10 ) );

	content
	.pipe( skip( 1 ) )
	.subscribe( c => {
		storage.setItem( 'content', c );
	} );

	content
	.pipe(
		map( str => ( new DOMParser ).parseFromString( str, 'image/svg+xml' ) ),
		tap( doc => {
			for( const node of traverse( doc.documentElement ) ) {
				if( node.nodeType === node.ELEMENT_NODE ) {
					const el = node as Element;
					if( el.localName === 'style' ) {
						try {
							const cssDom = new CssDom( el.textContent );
							while( el.firstChild ) el.removeChild( el.firstChild );
							el.appendChild( el.ownerDocument.createCDATASection( cssDom.stringify() ) );
						} catch {}
					}
				}
			}
		} ),
		map( doc => ( new XMLSerializer ).serializeToString( doc ) ),
		map( str => 'data:image/svg+xml;charset=utf-8;base64,' + base64.encode( str ) ),
		distinctUntilChanged(),
		retry( { delay: 100 } )
	)
	.subscribe( url => {
		( document.getElementById( 'image' ) as HTMLImageElement ).src = url;
		svgSource.setValue( url );
	} );
} )();
